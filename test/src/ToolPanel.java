import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class ToolPanel extends JPanel {

	int tool = 0;

	JPanel toolBoxPanel;
	ToolButton[] toolss;
	int selectedTool;
	final String[] iconPaths = { "pencil.png", "ereaser.png",
			"colorpicker.png", "bucket.png", "Rect.png", "Circle.png",
			"origin.png", "line.png", "clear.png", "normal.png" };
	ArrayList<DrawingModeListner> drawingListners;

	ToolPanel() {

		toolBoxPanel = new JPanel();
		toolBoxPanel.setPreferredSize(new Dimension(50, 180));
		toolBoxPanel.setLayout(new GridLayout(6, 2, 0, 0));
		toolBoxPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createEtchedBorder(),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)));
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createRaisedBevelBorder(),
				BorderFactory.createEmptyBorder(1, 0, 1, 0)));

		toolss = new ToolButton[11];
		for (int i = 0; i <= 9; i++) {
			toolss[i] = new ToolButton();
			toolss[i].tool = i;
			try {

				Image img = ImageIO.read(new File(ToolsIcon.folderPath
						+ ToolsIcon.path[i]));
				toolss[i].setBackground(new Color(238, 238, 238));
				toolss[i].setIcon(new ImageIcon(img));

				toolss[i].setBorder(BorderFactory.createEmptyBorder());
			} catch (IOException ex) {
			}
			toolss[i].addActionListener(new ActionListener() {

				@SuppressWarnings("static-access")
				@Override
				public void actionPerformed(ActionEvent e) {
					ToolButton tb = (ToolButton) e.getSource();
					tool = tb.tool;
					displaySelectedTool();
					processDrawingEvent(new DrawingModeEvent(tb.tool));

				}
			});

			toolBoxPanel.add(toolss[i]);
		}
		toolss[10] = new ToolButton();
		toolss[10].setBackground(new Color(238, 238, 238));
		displaySelectedTool();

		toolss[10].setBorder(BorderFactory.createEmptyBorder());

		toolBoxPanel.add(toolss[10]);

		this.add(toolBoxPanel);
		drawingListners = new ArrayList<DrawingModeListner>();

	}

	@SuppressWarnings("unchecked")
	private void processDrawingEvent(DrawingModeEvent e) {
		ArrayList<DrawingModeListner> temp;
		if (drawingListners.size() == 0)
			return;
		synchronized (this) {
			temp = (ArrayList<DrawingModeListner>) drawingListners.clone();

		}
		for (DrawingModeListner l : temp) {
			l.taskPerfomed(e);
		}

	}

	public synchronized void addDrawingListners(DrawingModeListner l) {
		if (!drawingListners.contains(l))
			drawingListners.add(l);
	}

	public synchronized void removeDrawingListners(DrawingModeListner l) {
		if (drawingListners.contains(l))
			drawingListners.remove(l);
	}

	public void displaySelectedTool() {
		try {

			Image img = ImageIO.read(new File(ToolsIcon.folderPath
					+ ToolsIcon.path[tool]));
			toolss[10].setIcon(new ImageIcon(img));

		} catch (Exception e) {

		}

	}
}

import java.io.File;

public class FileOperationEvent {
	public int operationMode;
	public File file;
	public int h;
	public int w;

	FileOperationEvent(int m, File f, int h, int w) {
		operationMode = m;
		file = f;
		this.h = h;
		this.w = w;
	}

}

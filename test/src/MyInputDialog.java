import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MyInputDialog extends JDialog {
	int datas[];
	JLabel wLabel;
	JLabel hLabel;

	JTextField wTextField;
	JTextField hTextField;

	JButton okButton;

	MyInputDialog(int args[]) {
		setModal(true);
		datas = args;
		setLayout(new FlowLayout());
		wLabel = new JLabel("Width : ");
		add(wLabel);
		wTextField = new JTextField(10);
		add(wTextField);
		hLabel = new JLabel("Height : ");
		add(hLabel);
		hTextField = new JTextField(10);
		add(hTextField);

		okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					datas[0] = Integer.parseInt(wTextField.getText());
					datas[1] = Integer.parseInt(hTextField.getText());
				} catch (Exception ex) {
					datas[0] = 1;
					datas[1] = 1;
				}

				dispose();

			}
		});
		add(okButton);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);

	}

}

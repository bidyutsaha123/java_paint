import java.awt.Point;

public class CursorsIcon {
	final public static String folderPath = "resources//cursors//";
	final public static String[] path = { "pencil.png", "ereaser.png",
			"colorpicker.png", "bucket.png", "Rect.png", "Circle.png",
			"triangle.png", "line.png", "clear.png", "undo.png" };
	final public static Point[] hotspot = { new Point(0, 13), new Point(0, 0),
			new Point(3, 12), new Point(2, 13), new Point(10, 10),
			new Point(10, 10), new Point(10, 10), new Point(10, 10),
			new Point(0, 0), new Point(0, 0) };

}

import java.awt.Point;

public class MyPoint extends Point {

	public MyPoint(MyPoint p) {
		this.x = p.x;
		this.y = p.y;

	}

	public MyPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MyPoint) {
			MyPoint p = (MyPoint) obj;

			if (this.x == p.x && this.y == p.y) {
				return true;

			} else {
				return false;
			}
		}
		return false;
	}

}

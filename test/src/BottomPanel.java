import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class BottomPanel extends JPanel implements ColorChngeListner {

	JPanel colorBox;
	ColorButton colors[];
	Color colorTray[];
	ColorButton sampleColor;
	public static JLabel statusBar_text;
	Color selectedColor;
	ArrayList<ColorChngeListner> colorChangeListners;

	BottomPanel() {
		this.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		this.setBorder(BorderFactory.createRaisedBevelBorder());
		colorTray = new Color[16];
		colorTray[0] = Color.black;
		colorTray[1] = Color.DARK_GRAY;
		colorTray[2] = Color.GRAY;
		colorTray[3] = Color.LIGHT_GRAY;
		colorTray[4] = Color.red;
		colorTray[5] = Color.MAGENTA;
		colorTray[6] = Color.pink;
		colorTray[7] = Color.orange;
		colorTray[8] = Color.ORANGE;
		colorTray[9] = Color.PINK;
		colorTray[10] = Color.PINK;
		colorTray[11] = Color.white;
		colorTray[12] = Color.YELLOW;
		colorTray[13] = Color.white;
		colorTray[14] = Color.WHITE;
		colorTray[15] = Color.white;

		colorBox = new JPanel();
		colorBox.setLayout(new GridLayout(2, 8, 1, 1));
		colorBox.setPreferredSize(new Dimension(184, 46));
		colorBox.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 20));
		colors = new ColorButton[16];
		for (int i = 0; i < 16; i++) {
			colors[i] = new ColorButton();

			colors[i].setBackground(colorTray[i]);
			colors[i].setColor(colorTray[i]);
			colors[i].setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createEmptyBorder(),
					BorderFactory.createLoweredSoftBevelBorder()));

			colorBox.add(colors[i]);
			colors[i].addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					ColorButton cb = (ColorButton) e.getSource();
					setSelectedColor(cb.getColor());

				}
			});
			colorChangeListners = new ArrayList<ColorChngeListner>();
		}
		sampleColor = new ColorButton();
		sampleColor.setBackground(Color.BLACK);
		sampleColor.setPreferredSize(new Dimension(36, 36));
		sampleColor.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		this.add(sampleColor);
		this.add(colorBox);

		statusBar_text = new JLabel();
		this.add(statusBar_text);

		selectedColor = Color.BLACK;

	}

	public Color getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
		sampleColor.setBackground(selectedColor);
		processColorChangeListners(new ColorChngeEvent(selectedColor));
	}

	public void setStatusBarText(String str) {
		statusBar_text.setText(str);
	}

	@Override
	public void colorChangePerfomed(ColorChngeEvent e) {

		setSelectedColor(e.color);

	}

	synchronized public void addColorChangeListner(ColorChngeListner l) {
		if (!colorChangeListners.contains(l)) {
			colorChangeListners.add(l);
		}
	}

	synchronized public void removeColorChangeListner(ColorChngeListner l) {
		if (colorChangeListners.contains(l)) {
			colorChangeListners.remove(l);
		}
	}

	@SuppressWarnings("unchecked")
	private void processColorChangeListners(ColorChngeEvent e) {

		ArrayList<ColorChngeListner> temp;
		synchronized (this) {
			if (colorChangeListners.size() == 0)
				return;
			temp = (ArrayList<ColorChngeListner>) colorChangeListners.clone();

		}
		for (ColorChngeListner l : temp)
			l.colorChangePerfomed(e);
	}

}

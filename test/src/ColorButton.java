import java.awt.Color;

import javax.swing.JButton;

public class ColorButton extends JButton {

	private Color color;

	ColorButton() {

		color = Color.black;

	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}

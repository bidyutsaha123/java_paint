import java.awt.AWTEvent;
import java.awt.Button;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;

public class ColorChangedDispatcher {
	private ArrayList<AWTEventListener> listners;

	public ColorChangedDispatcher() {
		listners = new ArrayList<AWTEventListener>();
	}

	public boolean addListners(AWTEventListener l) {
		JButton b = new JButton();
		//b.addActionListener(new ActionListener());
		return listners.add(l);

	}

	public boolean removeListners(AWTEventListener l) {
		return listners.remove(l);

	}

	@SuppressWarnings("unchecked")
	public void dispatch(Object sender, AWTEvent e) {
		ArrayList<AWTEventListener> temp;
		synchronized (this) {
			temp = (ArrayList<AWTEventListener>) listners.clone();

		}

		for (AWTEventListener awtEventListener : temp) {

		}
	}
}

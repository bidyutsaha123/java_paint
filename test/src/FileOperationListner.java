import java.io.File;

public interface FileOperationListner {

	public void createNewFile(int w, int h);

	public void openFile(File f);

	public void resizeCanvas(int w, int h);

}

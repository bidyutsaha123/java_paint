import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

public class Application extends JFrame {

	MenuPanel topPanel;
	ToolPanel leftPanel;
	PaintingPanel centerPanel;
	BottomPanel bottomPanel;

	public Application() {

		try {
			setIconImage(ImageIO.read(new File("resources//painticon.png")));
			//setDefaultLookAndFeelDecorated(LookAndFeelInfo.class.);
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			// getClass().getClassLoader().getResource("resources//painticon.png")
			// URL url = getClass().getClassLoader().getResource(
			// "resources/painticon.png");
			// setIconImage(ImageIO.read(url));

		} catch (IOException e1) {

			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setTitle("Paint Application");

		setExtendedState(JFrame.MAXIMIZED_BOTH); // for full screen
		setResizable(true); // for resize able
		System.out.println(getContentPane());
		topPanel = new MenuPanel();

		leftPanel = new ToolPanel();

		centerPanel = new PaintingPanel();
		// centerPanel.addMouseMotionListener(new MouseMotionListener() {
		//
		// @Override
		// public void mouseMoved(MouseEvent e) {
		// bottomPanel.setStatusBarText(" X=" + e.getPoint().x + " : Y="
		// + e.getPoint().y);
		//
		// }
		//
		// @Override
		// public void mouseDragged(MouseEvent e) {
		// bottomPanel.setStatusBarText(" X=" + e.getPoint().x + " : Y="
		// + e.getPoint().y);
		//
		// }
		// });

		bottomPanel = new BottomPanel();

		this.add(topPanel, BorderLayout.PAGE_START);
		this.add(leftPanel, BorderLayout.LINE_START);
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.PAGE_END);

		topPanel.addColorChangeListner(bottomPanel);
		topPanel.addColorChangeListner(centerPanel);
		bottomPanel.addColorChangeListner(centerPanel);
		centerPanel.addColorChangeListner(bottomPanel);

		leftPanel.addDrawingListners(centerPanel);

		topPanel.addfileOperationListners(centerPanel);

		centerPanel.addMouseListener(centerPanel);
		centerPanel.addMouseMotionListener(centerPanel);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Application apps = new Application();
				apps.setVisible(true);

				System.out.println();

			}
		});

	}

}

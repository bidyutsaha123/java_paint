import java.awt.AWTEvent;
import java.awt.Color;

public class ColorChangedEvent extends AWTEvent {
	public Color color;

	public ColorChangedEvent(Object source, int id, Color color) {
		super(source, id);
		this.color = color;
	}

}

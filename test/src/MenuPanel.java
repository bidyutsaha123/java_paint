import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

public class MenuPanel extends JPanel {
	JMenuBar menuBar;
	JMenu file;
	JMenuItem file_new;
	JMenuItem file_open;
	JMenuItem file_save;
	JMenuItem file_exit;

	JMenu color;
	JMenuItem color_editColor;
	JMenu help;
	JMenuItem help_about;

	ArrayList<ColorChngeListner> colorChangeListners;
	ArrayList<FileOperationListner> fileOperationListners;

	MenuPanel() {

		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createRaisedBevelBorder(),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));

		menuBar = new JMenuBar();

		file = new JMenu("File");
		file.setMnemonic('F');
		file.setIcon(new ImageIcon("resources//fileicon.png"));

		file_new = new JMenuItem("New");
		file_new.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				InputEvent.CTRL_MASK));
		file_new.setIcon(new ImageIcon("resources//newicon.png"));

		file_new.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int datas[] = new int[2];
				MyInputDialog md = new MyInputDialog(datas);

				processFileOperationListners(new FileOperationEvent(0, null,
						datas[0], datas[1]));

			}
		});

		file_open = new JMenuItem("Open");
		file_open.setIcon(new ImageIcon("resources//openicon.png"));
		file_open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
				InputEvent.CTRL_MASK));
		file_open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter jpgF = new FileNameExtensionFilter(
						"JPG file", "jpg");
				FileNameExtensionFilter pngF = new FileNameExtensionFilter(
						"PNG file", "png");
				FileNameExtensionFilter gifF = new FileNameExtensionFilter(
						"GIF file", "gif");
				fc.addChoosableFileFilter(jpgF);
				fc.addChoosableFileFilter(pngF);
				fc.addChoosableFileFilter(gifF);
				int result = fc.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedfile = fc.getSelectedFile();
					processFileOperationListners(new FileOperationEvent(1,
							selectedfile, 0, 0));
				}

			}
		});

		file_save = new JMenuItem("Save");
		file_save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				InputEvent.CTRL_MASK));
		file_save.setIcon(new ImageIcon("resources//saveicon.png"));
		file_save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter f = new FileNameExtensionFilter(
						"png files", "png");
				fc.addChoosableFileFilter(f);

				int result = fc.showSaveDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {

					String path = fc.getSelectedFile().getAbsolutePath();

					try {
						ImageIO.write(PaintingPanel.drawingPane,

						path.substring(path.lastIndexOf('.') + 1), new File(
								path));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			}
		});

		file_exit = new JMenuItem("Exit");
		file_exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
				InputEvent.CTRL_MASK));
		file_exit.setIcon(new ImageIcon("resources//exiticon.png"));
		file.add(file_new);
		file.add(file_open);
		file.add(file_save);
		file.add(file_exit);

		color = new JMenu("Color");
		color.setMnemonic('C');
		color.setIcon(new ImageIcon("resources//coloricon.png"));

		color_editColor = new JMenuItem("Edit Color ");
		color_editColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
				InputEvent.CTRL_MASK));
		color_editColor.setIcon(new ImageIcon("resources//morecoloricon.png"));
		color_editColor.addActionListener(new ActionListener() {

			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				JColorChooser cc = new JColorChooser();

				Color c = cc.showDialog(MenuPanel.this, "Select Color",
						Color.BLACK);
				if (c != null) {

					processMorecolorSelectionEvent(new ColorChngeEvent(c));
				}

			}
		});
		color.add(color_editColor);

		help = new JMenu("Help");
		help.setMnemonic('H');
		help.setIcon(new ImageIcon("resources//helpicon.png"));

		help_about = new JMenuItem("About Java Paint");
		help_about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
				InputEvent.CTRL_MASK));
		help_about.setIcon(new ImageIcon("resources//abouticon.png"));
		help_about.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane
						.showMessageDialog(null,
								"It is a Paint Application, written in Java for Fun by Bidyut Saha");

			}
		});
		help.add(help_about);

		menuBar.add(file);

		menuBar.add(color);

		menuBar.add(help);

		this.add(menuBar, BorderLayout.PAGE_START);
		colorChangeListners = new ArrayList<ColorChngeListner>();
		fileOperationListners = new ArrayList<FileOperationListner>();
	}

	synchronized public void addColorChangeListner(ColorChngeListner l) {
		if (!colorChangeListners.contains(l)) {
			colorChangeListners.add(l);
		}
	}

	synchronized public void removeColorChangeListner(ColorChngeListner l) {
		if (colorChangeListners.contains(l)) {
			colorChangeListners.remove(l);
		}
	}

	@SuppressWarnings("unchecked")
	private void processMorecolorSelectionEvent(ColorChngeEvent e) {
		ArrayList<ColorChngeListner> temp;
		synchronized (this) {
			if (colorChangeListners.size() == 0)
				return;
			temp = (ArrayList<ColorChngeListner>) colorChangeListners.clone();

		}
		for (ColorChngeListner l : temp)
			l.colorChangePerfomed(e);
	}

	synchronized public void addfileOperationListners(FileOperationListner l) {
		if (!fileOperationListners.contains(l)) {
			fileOperationListners.add(l);
		}
	}

	synchronized public void removefileOperationListners(FileOperationListner l) {
		if (fileOperationListners.contains(l)) {
			fileOperationListners.remove(l);
		}
	}

	@SuppressWarnings("unchecked")
	private void processFileOperationListners(FileOperationEvent e) {
		ArrayList<FileOperationListner> temp;
		synchronized (this) {
			if (colorChangeListners.size() == 0)
				return;
			temp = (ArrayList<FileOperationListner>) fileOperationListners
					.clone();

		}
		for (FileOperationListner l : temp) {
			if (e.operationMode == 0)
				l.createNewFile(e.w, e.h);
			else if (e.operationMode == 2)
				l.resizeCanvas(e.w, e.h);
			else if (e.operationMode == 1)
				l.openFile(e.file);

		}
	}
}

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PaintingPanel extends JPanel implements DrawingModeListner,
		ColorChngeListner, FileOperationListner, MouseMotionListener,
		MouseListener {
	int drawingMode;
	public static BufferedImage drawingPane;
	Graphics2D gr;
	Rectangle pageBoundary;
	BufferedImage temp;
	Color currentColor;
	boolean mousedown = false;
	Point start = new Point(0, 0);
	Point prevPoint;

	ArrayList<ColorChngeListner> colorChangeListners;

	PaintingPanel() {
		drawingMode = 0;
		setBackground(Color.DARK_GRAY);
		drawingPane = new BufferedImage(1000, 540, BufferedImage.TYPE_INT_ARGB);
		gr = drawingPane.createGraphics();

		initDrawingPane();
		currentColor = Color.BLACK;
		copy();
		changeCursor(drawingMode);
		colorChangeListners = new ArrayList<ColorChngeListner>();
		pageBoundary = new Rectangle(0, 0, drawingPane.getWidth(),
				drawingPane.getHeight());

	}

	private void graphicsAndBoundaryInit() {
		gr = drawingPane.createGraphics();
		pageBoundary = new Rectangle(0, 0, drawingPane.getWidth(),
				drawingPane.getHeight());
	}

	private void copy() {
		temp = copyImage(drawingPane);

	}

	public static BufferedImage copyImage(BufferedImage source) {
		BufferedImage b = new BufferedImage(source.getWidth(),
				source.getHeight(), source.getType());
		Graphics g = b.getGraphics();
		g.drawImage(source, 0, 0, null);
		g.dispose();
		return b;
	}

	private void drawTriangle(int x, int y, int w, int h) {
		gr.drawLine(x, y + h, x + w / 2, y);
		gr.drawLine(x + w / 2, y, x + w, y + h);
		gr.drawLine(x + w, y + h, x, y + h);

	}

	private void initDrawingPane() {
		gr.setColor(Color.WHITE);
		gr.fillRect(0, 0, drawingPane.getWidth(), drawingPane.getHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(drawingPane, 0, 0, null);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (pageBoundary.contains(e.getPoint()))

			BottomPanel.statusBar_text.setText(" X=" + e.getPoint().x + " : Y="
					+ e.getPoint().y);
		else {

			BottomPanel.statusBar_text.setText("");

		}

		Point end = e.getPoint();
		int x, y, w, h;
		gr.setColor(currentColor);
		switch (drawingMode) {
		// pencil
		case 0:

			gr.drawLine(prevPoint.x, prevPoint.y, e.getPoint().x,
					e.getPoint().y);
			prevPoint = e.getPoint();
			break;
		// ereaser
		case 1:
			gr.setColor(Color.white);

			erase(end);

			break;

		// rectangle
		case 4:
			gr.drawImage(temp, 0, 0, null);
			w = Math.abs(start.x - end.x);
			h = Math.abs(start.y - end.y);

			if (start.x <= end.x) {
				x = start.x;
			} else {
				x = end.x;
			}
			if (start.y <= end.y) {
				y = start.y;
			} else {
				y = end.y;
			}
			gr.drawRect(x, y, w, h);
			break;
		// oval
		case 5:
			gr.drawImage(temp, 0, 0, null);
			w = Math.abs(start.x - end.x);
			h = Math.abs(start.y - end.y);

			if (start.x <= end.x) {
				x = start.x;
			} else {
				x = end.x;
			}
			if (start.y <= end.y) {
				y = start.y;
			} else {
				y = end.y;
			}
			gr.drawOval(x, y, w, h);
			break;
		// triangle
		case 6:
			gr.drawImage(temp, 0, 0, null);
			w = Math.abs(start.x - end.x);
			h = Math.abs(start.y - end.y);

			if (start.x <= end.x) {
				x = start.x;
			} else {
				x = end.x;
			}
			if (start.y <= end.y) {
				y = start.y;
			} else {
				y = end.y;
			}
			drawTriangle(x, y, w, h);

			break;
		// line
		case 7:
			gr.drawImage(temp, 0, 0, null);
			gr.drawLine(start.x, start.y, end.x, end.y);
			break;

		default:
			break;
		}

		repaint();

	}

	private void erase(Point end) {
		gr.setColor(Color.white);

		for (int p = 0; p < 20; p++) {
			for (int q = 0; q <= 20; q++) {
				gr.drawRect(end.x + p, end.y + q, 1, 1);
			}
		}
	}

	private void undo() {

		gr.drawImage(temp, 0, 0, null);
		repaint();

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (pageBoundary.contains(e.getPoint()))

			BottomPanel.statusBar_text.setText(" X=" + e.getPoint().x + " : Y="
					+ e.getPoint().y);
		else {

			BottomPanel.statusBar_text.setText("");

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Point end = new Point(e.getPoint());

		if (drawingMode == 3)
			floodfill(new MyPoint(end.x, end.y),
					drawingPane.getRGB(end.x, end.y), currentColor.getRGB());
		else if (drawingMode == 2) {
			currentColor = new Color(drawingPane.getRGB(end.x, end.y));
			processColorChangeListners(new ColorChngeEvent(currentColor));

		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
		BottomPanel.statusBar_text.setText("");

	}

	@Override
	public void mousePressed(MouseEvent e) {

		mousedown = true;
		start = e.getPoint();
		prevPoint = e.getPoint();
		copy();

	}

	@Override
	public void mouseReleased(MouseEvent e) {

		mousedown = false;

	}

	private boolean test(int x, int y, int old) {
		int c = drawingPane.getRGB(x, y);
		if (c != old)
			return true;
		else {
			return false;
		}

	}

	// private void floodfill5(Point node, int targetColor, int
	// replacementColor) {
	// Stack<Point> points = new Stack<Point>();
	//
	// points.push(new Point(node));
	//
	// while (!points.isEmpty()) {
	//
	// Point currentNode = points.pop();
	// if (pageBoundary.contains(currentNode)
	// && (drawingPane.getRGB(currentNode.x, currentNode.y) == targetColor)) {
	// drawingPane.setRGB(currentNode.x, currentNode.y,
	// replacementColor);
	//
	// points.push(new Point(currentNode.x + 1, currentNode.y));
	// points.push(new Point(currentNode.x - 1, currentNode.y));
	// points.push(new Point(currentNode.x, currentNode.y + 1));
	// points.push(new Point(currentNode.x, currentNode.y - 1));
	//
	// }
	// }
	// System.out.println("flood filled complete");
	// repaint();
	//
	// }
	private void floodfill(MyPoint node, int targetColor, int replacementColor) {
		Stack<MyPoint> points = new Stack<MyPoint>();

		points.push(new MyPoint(node));

		while (!points.isEmpty()) {

			MyPoint currentNode = points.pop();
			if (pageBoundary.contains(currentNode)
					&& (drawingPane.getRGB(currentNode.x, currentNode.y) == targetColor)) {
				drawingPane.setRGB(currentNode.x, currentNode.y,
						replacementColor);
				MyPoint p;
				p = new MyPoint(currentNode.x + 1, currentNode.y);
				if (!points.contains(p))
					points.push(p);
				p = new MyPoint(currentNode.x - 1, currentNode.y);
				if (!points.contains(p))
					points.push(p);
				p = new MyPoint(currentNode.x, currentNode.y + 1);
				if (!points.contains(p))
					points.push(p);
				p = new MyPoint(currentNode.x, currentNode.y - 1);
				if (!points.contains(p))
					points.push(p);
				// points.push(new MyPoint(currentNode.x - 1, currentNode.y));
				// points.push(new MyPoint(currentNode.x, currentNode.y + 1));
				// points.push(new MyPoint(currentNode.x, currentNode.y - 1));

			}
		}
		System.out.println("flood filled complete");
		repaint();

	}

	private void clear() {
		gr.setColor(Color.WHITE);
		gr.fillRect(0, 0, drawingPane.getWidth(), drawingPane.getHeight());
		gr.setColor(currentColor);
		repaint();
	}

	private void changeCursor(int mode) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.getImage(CursorsIcon.folderPath
				+ CursorsIcon.path[mode]);
		Cursor c = toolkit.createCustomCursor(image, CursorsIcon.hotspot[mode],
				"img");

		this.setCursor(c);

	}

	@Override
	public void taskPerfomed(DrawingModeEvent d) {

		drawingMode = d.command;
		changeCursor(drawingMode);

		if (drawingMode == 9) {
			undo();
			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		} else if (drawingMode == 8) {
			clear();
			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}

	@Override
	public void colorChangePerfomed(ColorChngeEvent e) {
		currentColor = e.color;

	}

	synchronized public void addColorChangeListner(ColorChngeListner l) {
		if (!colorChangeListners.contains(l)) {
			colorChangeListners.add(l);
		}
	}

	synchronized public void removeColorChangeListner(ColorChngeListner l) {
		if (colorChangeListners.contains(l)) {
			colorChangeListners.remove(l);
		}
	}

	@SuppressWarnings("unchecked")
	private void processColorChangeListners(ColorChngeEvent e) {

		ArrayList<ColorChngeListner> temp;
		synchronized (this) {
			if (colorChangeListners.size() == 0)
				return;
			temp = (ArrayList<ColorChngeListner>) colorChangeListners.clone();

		}
		for (ColorChngeListner l : temp)
			l.colorChangePerfomed(e);
	}

	@Override
	public void createNewFile(int w, int h) {
		drawingPane = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		graphicsAndBoundaryInit();
		gr.setColor(Color.WHITE);
		gr.fillRect(0, 0, drawingPane.getWidth(), drawingPane.getHeight());
		gr.setColor(currentColor);
		repaint();

	}

	@Override
	public void openFile(File f) {
		try {
			drawingPane = ImageIO.read(f);
			graphicsAndBoundaryInit();
			repaint();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Override
	public void resizeCanvas(int w, int h) {

	}

}

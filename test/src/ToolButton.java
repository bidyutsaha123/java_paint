import javax.swing.JButton;

public class ToolButton extends JButton {
	int tool;

	/**
	 * @return the tool
	 */
	public int getTool() {
		return tool;
	}

	/**
	 * @param tool
	 *            the tool to set
	 */
	public void setTool(int tool) {
		this.tool = tool;
	}

}
